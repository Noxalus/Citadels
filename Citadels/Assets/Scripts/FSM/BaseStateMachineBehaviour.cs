﻿using UnityEngine;

public abstract class BaseStateMachineBehaviour<T> : StateMachineBehaviour
{
    #region Protected fields

    protected T _wrapper;
    protected Animator _animator;
    protected AnimatorStateInfo _stateInfo;
    protected int _layerIndex;

    #endregion

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        _animator = animator;
        _stateInfo = stateInfo;
        _layerIndex = layerIndex;

        _wrapper = animator.gameObject.GetComponent<T>();

        if (_wrapper == null)
        {
            Debug.LogError($"No {typeof(T)} component found for this behaviour.");
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
    }
}
