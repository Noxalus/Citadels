﻿using UnityEngine;

public class AssignCrownSMB : BaseStateMachineBehaviour<GameManager>
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        _wrapper.AssignCrown();
    }
}
