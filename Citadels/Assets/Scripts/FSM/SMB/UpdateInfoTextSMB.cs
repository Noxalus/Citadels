﻿using UnityEngine;

public class UpdateInfoTextSMB : BaseStateMachineBehaviour<GameManager>
{
    [SerializeField]
    private string _text = null;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        _wrapper.UpdateInfoText(_text);
    }
}
