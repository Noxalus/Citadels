﻿using UnityEngine;

public class WaitForPlayerRoleSelectionSMB : BaseStateMachineBehaviour<GameManager>
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        _wrapper.WaitForPlayerRoleSelection();
    }
}
