﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;
#endif

using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Class to use Animator Controller as finite state machine removing features specific to animation.
/// </summary>
[ExecuteInEditMode]
[AddComponentMenu("Citadels/FSM")]
[RequireComponent(typeof(Animator))]
public class FiniteStateMachine : MonoBehaviour
{
    #region Constants

    private const string NO_CONDITION_NAME = "NoCondition";

    #endregion

    #region Serialized fields

    /// <summary>
    /// Animator Controller reference representing the FSM
    /// </summary>
    [SerializeField]
    private RuntimeAnimatorController _fsm = null;

    /// <summary>
    /// Animator used for FSM logic
    /// </summary>
    [SerializeField, HideInInspector]
    private Animator _animator = null;

    #endregion

    private void OnEnable()
    {
        if (!_animator)
        {
            Debug.LogError($"The finite state machine on {name} is not linked to an Animator.");
            return;
        }

        SetRuntimeAnimatorController();
    }

    private void OnDestroy()
    {
        if (_animator == null)
        {
            return;
        }

        if (Application.isPlaying)
        {
            Destroy(_animator);
        }
        else
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode)
            {
                return;
            }

            // Delay call usage to ensure it's called only once
            EditorApplication.delayCall += () => DestroyImmediate(_animator);
#endif
        }
    }

    public bool GetBool(int id)
    {
        return _animator.GetBool(id);
    }

    public bool GetBool(string name)
    {
        return _animator.GetBool(name);
    }

    public float GetFloat(int id)
    {
        return _animator.GetFloat(id);
    }

    public float GetFloat(string name)
    {
        return _animator.GetFloat(name);
    }

    public int GetInteger(int id)
    {
        return _animator.GetInteger(id);
    }

    public int GetInteger(string name)
    {
        return _animator.GetInteger(name);
    }

    public void SetBool(int id, bool value)
    {
        _animator.SetBool(id, value);
    }

    public void SetBool(string name, bool value)
    {
        _animator.SetBool(name, value);
    }

    public void SetFloat(int id, float value)
    {
        _animator.SetFloat(id, value);
    }

    public void SetFloat(string name, float value)
    {
        _animator.SetFloat(name, value);
    }

    public void SetInteger(int id, int value)
    {
        _animator.SetInteger(id, value);
    }

    public void SetInteger(string name, int value)
    {
        _animator.SetInteger(name, value);
    }

    public void SetTrigger(int id)
    {
        _animator.SetTrigger(id);
    }

    public void ResetTrigger(int id)
    {
        _animator.ResetTrigger(id);
    }

    public int GetCurrentStateNameHash(int layer = 0)
    {
        int stateNameHash = _animator.GetCurrentAnimatorStateInfo(layer).fullPathHash;
        _animator.Play(stateNameHash, layer);
        return stateNameHash;
    }

    public void SetCurrentState(int stateNameHash, int layer = 0)
    {
        _animator.Play(stateNameHash, layer);
    }

    public List<AnimatorControllerParameter> GetParameters()
    {
        return _animator.parameters.ToList();
    }

    private void SetRuntimeAnimatorController()
    {
        if (_animator && _animator.runtimeAnimatorController != _fsm)
        {
            _animator.runtimeAnimatorController = _fsm;
        }
    }

#if UNITY_EDITOR
    private void SetParametersForFSM(Animator animator)
    {
        if (EditorApplication.isPlaying)
        {
            return;
        }

        AnimatorController animatorController = animator.runtimeAnimatorController as AnimatorController;

        if (animatorController == null)
        {
            return;
        }

        AnimatorControllerParameter[] animatorControllerParams = animatorController.parameters;
        int noConditionIndex = ArrayUtility.FindIndex(animatorControllerParams, acp => acp.name == NO_CONDITION_NAME);

        AnimatorControllerParameter noCondition = new AnimatorControllerParameter
        {
            name = NO_CONDITION_NAME,
            type = AnimatorControllerParameterType.Bool,
            defaultBool = true
        };

        if (noConditionIndex < 0)
        {
            ArrayUtility.Insert(ref animatorControllerParams, 0, noCondition);
        }
        else
        {
            animatorControllerParams[noConditionIndex] = noCondition;
        }

        animatorController.parameters = animatorControllerParams;
    }

    private void SetTransitionsForFSM(Animator animator)
    {
        if (EditorApplication.isPlaying)
        {
            return;
        }

        AnimatorController animatorController = animator.runtimeAnimatorController as AnimatorController;

        if (animatorController == null)
        {
            return;
        }

        foreach (AnimatorControllerLayer controllerLayer in animatorController.layers)
        {
            SetTransitionsInStateMachine(controllerLayer.stateMachine);
        }
    }

    private void SetTransitionsInStateMachine(AnimatorStateMachine statemachine)
    {
        ChildAnimatorState[] states = statemachine.states;
        foreach (ChildAnimatorState animatorState in states)
        {
            foreach (AnimatorStateTransition stateTransition in animatorState.state.transitions)
            {
                stateTransition.hasExitTime = false;
                stateTransition.duration = 0;
                stateTransition.exitTime = 0;

                if (stateTransition.conditions.Length == 0)
                {
                    stateTransition.AddCondition(AnimatorConditionMode.If, 0, NO_CONDITION_NAME);
                }
            }
        }

        foreach (ChildAnimatorStateMachine animatorStateMachine in statemachine.stateMachines)
        {
            SetTransitionsInStateMachine(animatorStateMachine.stateMachine);
        }
    }

    private List<ChildAnimatorState> GetStatesRecursive(AnimatorStateMachine stateMachine)
    {
        List<ChildAnimatorState> childAnimatorStateList = new List<ChildAnimatorState>();
        childAnimatorStateList.AddRange(stateMachine.states);

        for (int index = 0; index < stateMachine.stateMachines.Length; ++index)
        {
            childAnimatorStateList.AddRange(GetStatesRecursive(stateMachine.stateMachines[index].stateMachine));
        }

        return childAnimatorStateList;
    }

    private void OnGUI()
    {
        if (!_animator)
        {
            Debug.LogError($"The finite state machine on {name} is not linked to an Animator.");
            return;
        }

        SetRuntimeAnimatorController();

        SetParametersForFSM(_animator);
        SetTransitionsForFSM(_animator);
    }

    private void Awake()
    {
        // Animator component is automatically created when a
        // FiniteStateMachine is added as it uses the RequiredComponent annotation
        _animator = GetComponent<Animator>();
        _animator.hideFlags = HideFlags.HideInInspector;
    }
#endif
}
