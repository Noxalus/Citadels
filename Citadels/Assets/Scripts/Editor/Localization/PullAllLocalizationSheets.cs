﻿using System.Linq;
using UnityEditor;
using UnityEditor.Localization;
using UnityEditor.Localization.Plugins.Google;
using UnityEditor.Localization.Reporting;

public static class PullAllLocalizationSheets
{
    [MenuItem("Tools/Update Localization")]
    public static void Fetch()
    {
        foreach (string guid in AssetDatabase.FindAssets($"t: {nameof(StringTableCollection)}"))
        {
            StringTableCollection tableCollection = AssetDatabase.LoadAssetAtPath<StringTableCollection>(AssetDatabase.GUIDToAssetPath(guid));

            GoogleSheetsExtension sheet = tableCollection.Extensions.FirstOrDefault(extension => extension is GoogleSheetsExtension) as GoogleSheetsExtension;

            if (sheet == null)
            {
                continue;
            }

            GoogleSheets google = new GoogleSheets(sheet.SheetsServiceProvider)
            {
                SpreadSheetId = sheet.SpreadsheetId
            };

            google.PullIntoStringTableCollection(sheet.SheetId, sheet.TargetCollection as StringTableCollection, sheet.Columns, sheet.RemoveMissingPulledKeys, new ProgressBarReporter());

            EditorUtility.SetDirty(tableCollection);
        }

        AssetDatabase.SaveAssets();
    }
}