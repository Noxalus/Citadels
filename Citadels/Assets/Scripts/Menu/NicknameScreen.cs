﻿using TMPro;
using UnityEngine;

public class NicknameScreen : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField _inputField = null;

    [SerializeField]
    private PlayerData _playerData = null;

    public delegate void NicknameSelectionEvent();
    public event NicknameSelectionEvent OnNicknameChosen;

    public void Open()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void OnConfirm()
    {
        if (!string.IsNullOrEmpty(_inputField.text))
        {
            _playerData.SetNickname(_inputField.text);
            OnNicknameChosen?.Invoke();
            Close();
        }
    }
}
