﻿using System.IO;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private PlayerData _playerData = null;

    [SerializeField]
    private NicknameScreen _nicknameScreen = null;

    [SerializeField]
    private LobbyManager _lobbyManager = null;

    void Start()
    {
        _nicknameScreen.Close();

        SaveManager.Instance.Load();

        if (string.IsNullOrEmpty(_playerData.Nickname))
        {
            _nicknameScreen.OnNicknameChosen += _OnNicknameChosen;
            _nicknameScreen.Open();
        }
        else
        {
            _lobbyManager.Initialize();
        }
    }

    private void _OnNicknameChosen()
    {
        _nicknameScreen.OnNicknameChosen -= _OnNicknameChosen;
        _lobbyManager.Initialize();
    }
}
