﻿using System.Collections.Generic;

public class PlayerGameSessionData
{
    private string _userId;
    private string _nickname;
    private int _gold;
    private List<DistrictCard> _hand = new List<DistrictCard>();
    private RoleData _role;
    private bool _hasCrown;

    #region Properties

    public string UserId => _userId;
    public string Nickname => _nickname;
    public int Gold => _gold;
    public RoleData Role => _role;
    public bool HasCrown => _hasCrown;
    public int HandCount => _hand.Count;
    public List<DistrictCard> Hand => _hand;

    #endregion

    public PlayerGameSessionData(string userId, string nickname)
    {
        _userId = userId;
        _nickname = nickname;
        _gold = 0;
        _hand = new List<DistrictCard>();
        _role = null;
        _hasCrown = false;
    }

    public void RoundReset()
    {
        _role = null;
        _hand.Clear();
    }

    public void GiveCrown()
    {
        _hasCrown = true;
    }

    public void TakeCrown()
    {
        _hasCrown = false;
    }

    public void SetRole(RoleData role)
    {
        _role = role;
    }

    public void AddDistrictCard(DistrictCard district)
    {
        _hand.Add(district);
    }

    public void ChangeGold(int amount)
    {
        _gold += amount;
    }
}
