using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviourPunCallbacks, IPunObservable
{
    #region Constants

    private const int INITIAL_DISTRICT_CARDS_COUNT = 4;
    private const int KING_ORDER = 4;

    #region FSM

    private static readonly int FSM_GAME_STARTED_TRIGGER = Animator.StringToHash("GameStarted");
    private static readonly int FSM_ROLE_CARDS_INITIALIZED_TRIGGER = Animator.StringToHash("RoleCardsInitialized");
    private static readonly int FSM_DISTRICT_DECK_INITIALIZED_TRIGGER = Animator.StringToHash("DistrictDeckInitialized");
    private static readonly int FSM_ACTIVE_PLAYER_CHANGED_TRIGGER = Animator.StringToHash("ActivePlayerChanged");
    private static readonly int FSM_ACTIVE_PLAYER_PLAYED_TRIGGER = Animator.StringToHash("ActivePlayerPlayed");
    private static readonly int FSM_ALL_ROLES_FINISHED_TO_PLAY_TRIGGER = Animator.StringToHash("AllRolesFinishedToPlay");
    private static readonly int FSM_NEW_ROLE_ASSIGNED_TRIGGER = Animator.StringToHash("NewRoleAssigned");
    private static readonly int FSM_NEW_ROUND_INITIALIZED_TRIGGER = Animator.StringToHash("NewRoundInitialized");
    private static readonly int FSM_DISTRICT_CARDS_DEALT_TRIGGER = Animator.StringToHash("DistrictCardsDealt");
    private static readonly int FSM_IS_CROWN_GIVEN = Animator.StringToHash("IsCrownGiven");
    private static readonly int FSM_REMAINING_ROLES_TO_ASSIGN_COUNT = Animator.StringToHash("RemainingRolesToAssignCount");

    
    #endregion

    #endregion

    #region Serialized fields

    [Header("Inner references")]

    [SerializeField]
    private TMP_Text _infoText = null;

    [SerializeField]
    private Text _timerText = null;

    [SerializeField]
    private PhotonView _photonView = null;

    [SerializeField]
    private DebugManager _debugManager = null;

    // TODO: Create a UIManager class to clean up this mess

    [SerializeField]
    private GameBoardView _boardView = null;

    [SerializeField]
    private PlayerHandView _playerHand = null;

    [SerializeField]
    private RoleSelectionView _roleSelectionView = null;

    [SerializeField]
    private DistrictCardPreview _districtCardPreview = null;

    [Header("Assets")]

    [SerializeField]
    private GameDatabase _database = null;

    [SerializeField]
    private FiniteStateMachine _fsm = null;

    [SerializeField]
    private GameConfigurationData _gameConfiguration = null;

    [Header("Debug")]

    [SerializeField]
    private int _botCount = 0;

    [SerializeField]
    private bool _debugIsKing = false;

    #endregion

    #region Private fields
    
    // Game session data
    private List<PlayerGameSessionData> _players = new List<PlayerGameSessionData>();
    private List<RoleData> _availableRoles = new List<RoleData>();

    // Round session data
    private List<RoleData> _remainingRoles = new List<RoleData>();
    private List<RoleData> _excludedRoles = new List<RoleData>();
    private RoleData _hiddenRole;
    private int _currentRoleOrder = 0;
    private PlayerGameSessionData _activePlayer;
    private DistrictDeck _districtCardsDeck;

    #endregion

    #region Properties

    public GameDatabase Database => _database;
    public bool IsActivePlayer => PhotonNetwork.LocalPlayer.UserId == _activePlayer.UserId;
    public PlayerGameSessionData ActivePlayer => _activePlayer;

    public PlayerGameSessionData Me => _players.First(p => p.UserId == PhotonNetwork.LocalPlayer.UserId);

    #endregion

    public override void OnEnable()
    {
        base.OnEnable();

        CountdownTimer.OnCountdownTimerHasExpired += OnCountdownTimerIsExpired;
        _boardView.OnEndTurn += OnEndTurn;
        _roleSelectionView.OnRoleSelected += RequestRoleAssignation;
    }

    public override void OnDisable()
    {
        base.OnDisable();

        CountdownTimer.OnCountdownTimerHasExpired -= OnCountdownTimerIsExpired;
        _boardView.OnEndTurn -= OnEndTurn;
        _roleSelectionView.OnRoleSelected -= RequestRoleAssignation;
    }

    public void Start()
    {
        UpdateInfoText("Waiting for other players...");
        _timerText.text = "";

        Hashtable props = new Hashtable {
            {GameSettings.PLAYER_LOADED_LEVEL, true}
        };

        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
    }

    public PlayerGameSessionData GetPlayer(string userId)
    {
        return _players.First(p => p.UserId == userId);
    }

    #region PUN callbacks

    public override void OnDisconnected(DisconnectCause cause)
    {
        SceneManager.LoadScene("LobbyScene");
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.Disconnect();
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }

        if (changedProps.ContainsKey(GameSettings.PLAYER_LOADED_LEVEL))
        {
            if (CheckAllPlayerLoadedLevel())
            {
                Debug.Log("All players have loaded the game scene!");

                Hashtable props = new Hashtable
                {
                    {CountdownTimer.CountdownStartTime, (float) PhotonNetwork.Time}
                };

                PhotonNetwork.CurrentRoom.SetCustomProperties(props);
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
    }

    #endregion

    private bool CheckAllPlayerLoadedLevel()
    {
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            object playerLoadedLevel;

            if (player.CustomProperties.TryGetValue(GameSettings.PLAYER_LOADED_LEVEL, out playerLoadedLevel))
            {
                if ((bool)playerLoadedLevel)
                {
                    continue;
                }
            }

            return false;
        }

        return true;
    }

    private void OnCountdownTimerIsExpired()
    {
        StartGame();
    }

    private void StartGame()
    {
        Reset();

        UpdateInfoText("Start game!");

        foreach (Player onlinePlayer in PhotonNetwork.PlayerList)
        {
            Debug.Log($"Create player data for {onlinePlayer.NickName} (user Id = {onlinePlayer.UserId})");

            PlayerGameSessionData playerSessionData = new PlayerGameSessionData(onlinePlayer.UserId, onlinePlayer.NickName);
            _players.Add(playerSessionData);
        }

        for (int i = 1; i <= _botCount; i++)
        {
            PlayerGameSessionData playerSessionData = new PlayerGameSessionData(i.ToString(), $"Bot{i}");
            _players.Add(playerSessionData);
        }

        for (int i = 1; i <= _gameConfiguration.RolesCount; i++)
        {
            _availableRoles.Add(_gameConfiguration.GetRole(i));
        }

        _boardView.Initialize(_players, _availableRoles);
        _playerHand.Initialize(Me);

        _playerHand.OnRequestCardPreview += OnRequestDistrictCardPreview;

        _fsm.SetTrigger(FSM_GAME_STARTED_TRIGGER);
    }

    private void Reset()
    {
        _players.Clear();
        _availableRoles.Clear();
        _remainingRoles.Clear();
        _excludedRoles.Clear();
        _hiddenRole = null;
        _districtCardsDeck = new DistrictDeck();

        _currentRoleOrder = 0;
        _activePlayer = null;

        _roleSelectionView.Reset();
        _boardView.Reset();

        _fsm.SetInteger(FSM_REMAINING_ROLES_TO_ASSIGN_COUNT, PhotonNetwork.PlayerList.Count());
        _fsm.SetBool(FSM_IS_CROWN_GIVEN, false);
    }

    #region FSM methods

    public void AssignCrown()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            var kingPlayer = _players[UnityEngine.Random.Range(0, _players.Count)];

#if DEBUG
            if (_debugIsKing)
            {
                kingPlayer = _players[0];
            }
#endif

            _photonView.RPC("RPCSetKing", RpcTarget.AllViaServer, kingPlayer.UserId);
        }
    }

    public void InitializeRoleCards()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            var remainingRoles = new List<RoleData>(_availableRoles);
            var excludedRoles = new List<RoleData>();

            var kingRole = remainingRoles.FirstOrDefault(r => r.Rank == KING_ORDER);

            if (kingRole != null)
            {
                remainingRoles.Remove(kingRole);
            }

            int rolesToExcludeCount = remainingRoles.Count - _players.Count;
            for (int i = 0; i < rolesToExcludeCount; i++)
            {
                var roleToExclude = remainingRoles[UnityEngine.Random.Range(0, remainingRoles.Count)];
                
                excludedRoles.Add(roleToExclude);
                remainingRoles.Remove(roleToExclude);
            }

            if (kingRole != null)
            {
                remainingRoles.Add(kingRole);
            }

            string[] remainingRoleIds = remainingRoles.Select(role => role.Id).ToArray();
            string[] excludedRoleIds = excludedRoles.Select(role => role.Id).ToArray();

            _photonView.RPC("RPCInitializeRoleCards", RpcTarget.AllViaServer, remainingRoleIds, excludedRoleIds);
        }
    }

    public void InitializeDistrictDeck()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            DistrictDeck districtCardsDeck = new DistrictDeck(_gameConfiguration.GetDistrictCardsDeck().GetAllCards());
            districtCardsDeck.Shuffle();

            var json = districtCardsDeck.ToJson();

            _photonView.RPC("RPCInitializeDistrictDeck", RpcTarget.AllViaServer, json);
        }
    }

    public void DealDistrictCards()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            foreach (var player in _players)
            {
                var cards = _districtCardsDeck.DrawCards(INITIAL_DISTRICT_CARDS_COUNT);
                var cardsInstanceIds = cards.Select(card => card.InstanceId).ToArray();

                _districtCardsDeck.DealCardsToPlayer(cardsInstanceIds.ToList(), player);
                _boardView.RefreshPlayerBoard(player.UserId);
             
                _photonView.RPC("RPCDealDistrictCardsToPlayer", RpcTarget.AllViaServer, cardsInstanceIds, player.UserId);
            }

            _fsm.SetTrigger(FSM_DISTRICT_CARDS_DEALT_TRIGGER);
        }
    }

    public void SwitchToNextPlayer()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            string activePlayer;

            if (_activePlayer == null)
            {
                activePlayer = _players.First(player => player.HasCrown).UserId;
            }
            else
            {
                int currentIndex = _players.IndexOf(_activePlayer);
                activePlayer = _players[(currentIndex + 1) % _players.Count].UserId;
            }

            _photonView.RPC("RPCSetActivePlayer", RpcTarget.AllViaServer, activePlayer);
        }
    }

    public void SwitchToNextRole()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PlayerGameSessionData activePlayer = null;
            int currentRole = _currentRoleOrder;

            do
            {
                currentRole++;
                activePlayer = _players.FirstOrDefault(p => p.Role.Rank == currentRole);
            } while (activePlayer == null && currentRole <= _gameConfiguration.RolesCount);

            _photonView.RPC(
                "RPCSetActiveRole", 
                RpcTarget.AllViaServer, 
                currentRole, 
                activePlayer != null ? activePlayer.UserId : ""
            );
        }
    }

    public void WaitForPlayerRoleSelection()
    {
        if (_activePlayer == null)
        {
            Debug.LogError("NO ACTIVE PLAYER");
        }

        if (IsActivePlayer)
        {
            ShowRoleSelection();
        }
        else
        {
            UpdateInfoText($"Waiting for {ActivePlayer.Nickname} to choose his role...");
        }
    }

    public void WaitForActivePlayerToPlay()
    {
        if (IsActivePlayer)
        {
            ShowGameplayUI();
            UpdateInfoText("Your turn to play!");
        }
        else
        {
            ShowGameplayUI(false);
            UpdateInfoText($"Waiting for {ActivePlayer.Nickname} to play his role of {ActivePlayer.Role.Id}...");
        }
    }

    public void UpdateInfoText(string text)
    {
        _infoText.text = text;
    }

    public void StartNewRound()
    {
        foreach (var player in _players)
        {
            player.RoundReset();
            _boardView.RefreshPlayerBoard(player.UserId);
        }

        _currentRoleOrder = 0;
        _activePlayer = null;
        _remainingRoles.Clear();
        _excludedRoles.Clear();
        _hiddenRole = null;

        _fsm.SetInteger(FSM_REMAINING_ROLES_TO_ASSIGN_COUNT, PhotonNetwork.PlayerList.Count());

        _fsm.ResetTrigger(FSM_ACTIVE_PLAYER_CHANGED_TRIGGER);
        _fsm.ResetTrigger(FSM_ACTIVE_PLAYER_PLAYED_TRIGGER);
        _fsm.ResetTrigger(FSM_ALL_ROLES_FINISHED_TO_PLAY_TRIGGER);

        _fsm.SetTrigger(FSM_NEW_ROUND_INITIALIZED_TRIGGER);

        UpdateInfoText("Start a new round");
    }

    #endregion

    #region RPC methods

    [PunRPC]
    private void RPCSetKing(string userId)
    {
        PlayerGameSessionData player = GetPlayer(userId);

        var kingPlayer = GetPlayer(userId);
        kingPlayer.GiveCrown();
        _boardView.RefreshPlayerBoard(userId);

        _fsm.SetBool(FSM_IS_CROWN_GIVEN, true);

        var message = $"{player.Nickname} is the new king.";

        UpdateInfoText(message);
        _debugManager.AddMessage(message);
    }

    [PunRPC]
    private void RPCInitializeRoleCards(string[] remainingRoles, string[] excludedRoles)
    {
        _remainingRoles.Clear();
        _excludedRoles.Clear();

        foreach (var roleId in remainingRoles)
        {
            _remainingRoles.Add(_database.GetRole(roleId));
        }

        foreach (var roleId in excludedRoles)
        {
            _excludedRoles.Add(_database.GetRole(roleId));
        }

        _fsm.SetTrigger(FSM_ROLE_CARDS_INITIALIZED_TRIGGER);
        _debugManager.AddMessage("Role cards have been initialized");
    }

    [PunRPC]
    private void RPCInitializeDistrictDeck(string jsonData)
    {
        var districtDeckJson = JsonUtility.FromJson<DistrictCardsDeckJSON>(jsonData);
        _districtCardsDeck.FromJson(districtDeckJson, _database);

        _fsm.SetTrigger(FSM_DISTRICT_DECK_INITIALIZED_TRIGGER);
        _debugManager.AddMessage("District deck has been initialized");
    }

    [PunRPC]
    private void RPCSetActivePlayer(string userId)
    {
        Debug.Log("RPCSetActivePlayer");
        SetActivePlayer(userId);
    }

    [PunRPC]
    private void RPCSetActiveRole(int roleOrder, string activePlayerUserId)
    {
        Debug.Log("RPCSetActiveRole");

        if (roleOrder > _gameConfiguration.RolesCount)
        {
            _fsm.SetTrigger(FSM_ALL_ROLES_FINISHED_TO_PLAY_TRIGGER);
            return;
        }

        _currentRoleOrder = roleOrder;
        _boardView.SetActiveCharacterToken(roleOrder);

        if (!string.IsNullOrEmpty(activePlayerUserId))
        {
            SetActivePlayer(activePlayerUserId);
            _boardView.RefreshPlayerBoard(activePlayerUserId);
        }

        Debug.Log($"Set active role to {roleOrder} => active player is {activePlayerUserId}");
    }

    [PunRPC]
    private void RPCRequestRoleAssignation(string roleId, string userId)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if (userId == _activePlayer.UserId && _remainingRoles.FirstOrDefault(role => role.Id == roleId) != null)
            {
                _photonView.RPC("RPCAssignRole", RpcTarget.AllViaServer, roleId, userId);
            }
        }
    }

    [PunRPC]
    private void RPCAssignRole(string roleId, string userId)
    {
        PlayerGameSessionData player = GetPlayer(userId);
        RoleData role = _database.GetRole(roleId);

        player.SetRole(role);
        _remainingRoles.Remove(role);

        // Make sure to close the UI
        _roleSelectionView.Close();

        int remainingRolesToAssignCount = _remainingRoles.Count - 1;

        if (remainingRolesToAssignCount == 0)
        {
            var remainingRole = _remainingRoles[0];
            _hiddenRole = remainingRole;
            _remainingRoles.Remove(remainingRole);
        }

        _fsm.SetInteger(FSM_REMAINING_ROLES_TO_ASSIGN_COUNT, remainingRolesToAssignCount);
        _fsm.SetTrigger(FSM_NEW_ROLE_ASSIGNED_TRIGGER);

        _debugManager.AddMessage($"Assign role {role.Id} to {player.Nickname}");
    }

    [PunRPC]
    private void RPCRequestEndTurn(string userId)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if (userId == _activePlayer.UserId)
            {
                _photonView.RPC("RPCEndTurn", RpcTarget.AllViaServer, userId);
            }
        }
    }

    [PunRPC]
    private void RPCEndTurn(string userId)
    {
        var previousPlayer = GetPlayer(userId);
        var message = $"{previousPlayer.Nickname} finished his turn!";
        
        UpdateInfoText(message);
        _debugManager.AddMessage(message);

        _fsm.SetTrigger(FSM_ACTIVE_PLAYER_PLAYED_TRIGGER);
    }

    [PunRPC]
    private void RPCDealDistrictCardsToPlayer(string[] cardsInstanceIds, string userId)
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            var player = GetPlayer(userId);
            _districtCardsDeck.DealCardsToPlayer(cardsInstanceIds.ToList(), player);
            _boardView.RefreshPlayerBoard(userId);

            var cards = string.Join(",", player.Hand.Select(card => card.Data.Id));
            _debugManager.AddMessage($"Player {player.Nickname} has drew these cards: {cards}");

            if (_players.All(p => p.HandCount == INITIAL_DISTRICT_CARDS_COUNT))
            {
                _fsm.SetTrigger(FSM_DISTRICT_CARDS_DEALT_TRIGGER);
            }
        }

        _playerHand.Refresh();
    }

    #endregion

    private void SetActivePlayer(string userId)
    {
        _activePlayer = GetPlayer(userId);

        Debug.Log("Set active player");

        _fsm.SetTrigger(FSM_ACTIVE_PLAYER_CHANGED_TRIGGER);
        _debugManager.AddMessage($"Set active player: {_activePlayer.Nickname}");
    }

    private void ShowRoleSelection()
    {
        _roleSelectionView.Initialize(_remainingRoles);
        _roleSelectionView.Open();
    }

    private void ShowGameplayUI(bool show = true)
    {
        _boardView.ShowGameplayUI(show);
    }

    private void RequestRoleAssignation(RoleData role)
    {
        _photonView.RPC("RPCRequestRoleAssignation", RpcTarget.MasterClient, role.Id, PhotonNetwork.LocalPlayer.UserId);
    }

    private void OnEndTurn()
    {
        ShowGameplayUI(false);

        _photonView.RPC("RPCRequestEndTurn", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.UserId);
    }

    private void OnRequestDistrictCardPreview(DistrictCardData districtCardData)
    {
        _districtCardPreview.Show(districtCardData);
    }
}
