﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(HorizontalLayoutGroup))]
public class HorizontalCardHolderView : MonoBehaviour
{
    #region Serialized fields

    [SerializeField]
    private HorizontalLayoutGroup _layout = null;

    [SerializeField]
    private float _spacingPercentage = 0.1f;

    #endregion

    private void OnRectTransformDimensionsChange()
    {
        RefreshLayout();
    }

    private void OnTransformChildrenChanged()
    {
        RefreshLayout();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            RefreshLayout();
        }
    }

    public void RefreshLayout()
    {
        if (_layout == null)
        {
            return;
        }

        int count = _layout.transform.childCount;

        if (count == 0)
        {
            return;
        }

        var rect = (transform as RectTransform).rect;
        float maxWidth = rect.width;
        float maxHeight = rect.height;

        var lastChild = _layout.transform.GetChild(count - 1);
        var aspectRatioFitter = lastChild.GetComponent<AspectRatioFitter>();

        if (aspectRatioFitter == null)
        {
            return;
        }

        float width = maxWidth / (count + (count * _spacingPercentage) + _spacingPercentage);
        float spacingPixels = width * _spacingPercentage;

        maxHeight = Mathf.Min((1f / aspectRatioFitter.aspectRatio) * width, maxHeight);

        _layout.spacing = spacingPixels;

        foreach (Transform child in _layout.transform)
        {
            var rectTransform = child as RectTransform;
            rectTransform.sizeDelta = new Vector2(rectTransform.rect.width, maxHeight);
        }
    }
}
