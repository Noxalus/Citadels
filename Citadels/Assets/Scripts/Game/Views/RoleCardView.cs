﻿using TMPro;
using UnityEngine;
using UnityEngine.Localization.Components;

public class RoleCardView : MonoBehaviour
{
    #region Constants

    private static readonly int ANIMATOR_IS_VISIBLE = Animator.StringToHash("IsVisible");

    #endregion

    #region Events

    public delegate void RoleCardViewEvent(RoleData roleData);
    public event RoleCardViewEvent OnSelected;

    #endregion

    #region Serialized fields

    [SerializeField]
    private TMP_Text _rankText = null;

    [SerializeField]
    private Animator _animator = null;

    [SerializeField]
    private LocalizeStringEvent _nameLocalization = null;

    [SerializeField]
    private LocalizeStringEvent _descriptionLocalization = null;

    #endregion

    #region Private fields

    private RoleData _data;

    #endregion

    private void Awake()
    {
        if (_data != null)
        {
            Initialize(_data);
        }
    }

    public void Initialize(RoleData data)
    {
        if (data == null)
        {
            return;
        }

        _data = data;

        if (_animator.isInitialized && _animator.gameObject.activeSelf)
        {
            _animator.SetBool(ANIMATOR_IS_VISIBLE, _data != null);
        }

        RefreshUI();
    }

    private void RefreshUI()
    {
        _rankText.text = _data.Rank.ToString();
        _nameLocalization.StringReference.TableEntryReference = $"ROLE_{_data.Id}_NAME";
        _descriptionLocalization.StringReference.TableEntryReference = $"ROLE_{_data.Id}_DESCRIPTION";
    }

    public void OnClick()
    {
        OnSelected?.Invoke(_data);
    }
}