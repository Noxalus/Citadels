﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameBoardView : MonoBehaviour
{
    #region Events

    public delegate void GameBoardEvent();
    public event GameBoardEvent OnEndTurn;

    #endregion

    #region Serialized fields

    [SerializeField]
    private RectTransform _playerBoardsTopHolder = null;

    [SerializeField]
    private RectTransform _playerBoardsBottomHolder = null;

    [SerializeField]
    private RectTransform _characterTokensHolder = null;

    [SerializeField]
    private Button _endTurnButton = null;

    [Header("Assets")]

    [SerializeField]
    private PlayerBoardView _playerBoardPrefab = null;

    [SerializeField]
    private CharacterTokenView _characterTokenPrefab = null;

    #endregion

    #region Private fields

    private List<PlayerBoardView> _playerBoardInstances = new List<PlayerBoardView>();
    private List<CharacterTokenView> _characterTokenInstances = new List<CharacterTokenView>();
    private CharacterTokenView _activeCharacterToken = null;

    #endregion

    private void Start()
    {
        ShowGameplayUI(false);
    }

    public void Initialize(List<PlayerGameSessionData> players, List<RoleData> roles)
    {
        Reset();

        bool instantiateToBottom = true;
        foreach (var player in players)
        {
            var playerBoard = Instantiate(_playerBoardPrefab, instantiateToBottom ? _playerBoardsBottomHolder : _playerBoardsTopHolder);
            playerBoard.Initialize(player);

            instantiateToBottom = !instantiateToBottom;

            _playerBoardInstances.Add(playerBoard);
        }

        foreach (var role in roles)
        {
            var characterToken = Instantiate(_characterTokenPrefab, _characterTokensHolder);
            characterToken.Initialize(role);

            _characterTokenInstances.Add(characterToken);
        }
    }

    public void Reset()
    {
        foreach (var instance in _playerBoardInstances)
        {
            Destroy(instance.gameObject);
        }

        _playerBoardInstances.Clear();

        foreach (var instance in _characterTokenInstances)
        {
            Destroy(instance.gameObject);
        }

        _characterTokenInstances.Clear();

        ShowGameplayUI(false);
    }

    public void RefreshPlayerBoard(string userId)
    {
        var playerBoard = _playerBoardInstances.First(board => board.Player.UserId == userId);
        playerBoard.RefreshUI();
    }

    public void SetActiveCharacterToken(int roleOrder)
    {
        if (_activeCharacterToken)
        {
            _activeCharacterToken.Highlight(false);
        }

        _activeCharacterToken = _characterTokenInstances.First(token => token.Data.Rank == roleOrder);
        _activeCharacterToken.Highlight();
    }

    public void ShowGameplayUI(bool show = true)
    {
        if (show)
        {
            _endTurnButton.onClick.AddListener(OnEndTurnButtonClick);
            _endTurnButton.gameObject.SetActive(true);
        }
        else
        {
            _endTurnButton.onClick.RemoveListener(OnEndTurnButtonClick);
            _endTurnButton.gameObject.SetActive(false);
        }
    }

    private void OnEndTurnButtonClick()
    {
        OnEndTurn?.Invoke();
    }
}