﻿using TMPro;
using UnityEngine;

public class CharacterTokenView : MonoBehaviour
{
    [SerializeField]
    private TMP_Text _orderText = null;

    private RoleData _data;

    public RoleData Data => _data;

    public void Initialize(RoleData roleData)
    {
        _data = roleData;

        RefreshUI();
    }

    private void RefreshUI()
    {
        _orderText.text = _data.Rank.ToString();
    }

    public void Highlight(bool highlight = true)
    {
        _orderText.color = (highlight) ? Color.red : Color.white;
    }
}
