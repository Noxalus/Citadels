﻿using UnityEngine;

public class DistrictCardPreview : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup _canvasGroup = null;

    [SerializeField]
    private DistrictCardView _districtCard = null;

    private void Awake()
    {
        Hide();
    }

    public void Show(DistrictCardData districtCardData)
    {
        _districtCard.Initialize(districtCardData);

        _canvasGroup.alpha = 1f;
        _canvasGroup.interactable = true;
        _canvasGroup.blocksRaycasts = true;
    }

    public void Hide()
    {
        _canvasGroup.alpha = 0f;
        _canvasGroup.interactable = false;
        _canvasGroup.blocksRaycasts = false;
    }
}
