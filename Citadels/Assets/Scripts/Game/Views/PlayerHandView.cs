﻿using Boo.Lang;
using UnityEngine;
using static DistrictCardView;

public class PlayerHandView : MonoBehaviour
{
    public event DistrictCardViewEvent OnRequestCardPreview;

    #region Serialized fields

    [Header("Inner references")]

    [SerializeField]
    private HorizontalCardHolderView _cardsHolder = null;

    [Header("Assets")]
    
    [SerializeField]
    private DistrictCardView _districtCardPrefab = null;

    #endregion

    #region Private fields

    private PlayerGameSessionData _player;
    private List<DistrictCardView> _districtCardInstances = new List<DistrictCardView>();

    #endregion

    public void Initialize(PlayerGameSessionData player)
    {
        _player = player;

        Refresh();
    }

    public void Reset()
    {
        foreach (var instance in _districtCardInstances)
        {
            instance.OnRequestPreview -= OnRequestPreview;
            Destroy(instance.gameObject);
        }

        _districtCardInstances.Clear();
    }

    public void Refresh()
    {
        Reset();

        foreach (var districtCard in _player.Hand)
        {
            var districtCardView = Instantiate(_districtCardPrefab, _cardsHolder.transform);
            districtCardView.Initialize(districtCard.Data);
            districtCardView.RefreshCostPart();

            districtCardView.OnRequestPreview += OnRequestPreview;

            _districtCardInstances.Add(districtCardView);
        }

        _cardsHolder.RefreshLayout();
    }

    private void OnRequestPreview(DistrictCardData data)
    {
        OnRequestCardPreview?.Invoke(data);
    }
}
