﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoleSelectionView : MonoBehaviour
{
    #region Events

    public delegate void RoleSelectionEvent(RoleData role);
    public event RoleSelectionEvent OnRoleSelected;

    #endregion

    #region Serialized fields

    [Header("Inner references")]

    [SerializeField]
    private HorizontalCardHolderView _roleCardsHolder = null;

    [Header("Assets")]

    [SerializeField]
    private RoleCardView _roleCardPrefab = null;

    #endregion

    #region Private fields

    private List<RoleCardView> _roleCardInstances = new List<RoleCardView>();

    #endregion

    public void Initialize(List<RoleData> remainingRoles)
    {
        Reset();

        foreach (var role in remainingRoles)
        {
            var roleCard = Instantiate(_roleCardPrefab, _roleCardsHolder.transform);
            roleCard.Initialize(role);
            roleCard.OnSelected += OnRoleCardSelected;

            _roleCardInstances.Add(roleCard);
        }

        _roleCardsHolder.RefreshLayout();
    }

    private void OnRoleCardSelected(RoleData role)
    {
        OnRoleSelected?.Invoke(role);
    }

    public void Reset()
    {
        foreach (var instance in _roleCardInstances)
        {
            instance.OnSelected -= OnRoleCardSelected;
            Destroy(instance.gameObject);
        }

        _roleCardInstances.Clear();

        Close();
    }

    public void Open()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }
}
