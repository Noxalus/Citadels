﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Localization.Components;
using UnityEngine.UI;

[ExecuteInEditMode]
public class DistrictCardView : MonoBehaviour, IPointerClickHandler
{
    #region Constants

    private static readonly int ANIMATOR_IS_VISIBLE = Animator.StringToHash("IsVisible");

    #endregion

    #region Events

    public delegate void DistrictCardViewEvent(DistrictCardData data);
    public event DistrictCardViewEvent OnSelected;
    public event DistrictCardViewEvent OnRequestPreview;

    #endregion

    #region Serialized fields

    [Header("Configuration")]

    [SerializeField]
    private float _costSpacingPercentage = -0.1f;

    [Header("Inner references")]

    [SerializeField]
    private LocalizeStringEvent _nameLocalization = null;

    [SerializeField]
    private LocalizeStringEvent _descriptionLocalization = null;

    [SerializeField]
    private VerticalLayoutGroup _costHolder = null;

    [SerializeField]
    private Animator _animator = null;

    [Header("Assets")]

    [SerializeField]
    private GoldCoinView _goldCoinPrefab = null;

    #endregion

    #region Private fields

    private DistrictCardData _data;
    private List<GoldCoinView> _goldCoinInstances = new List<GoldCoinView>();

    #endregion

    private void Awake()
    {
        if (_data != null)
        {
            Initialize(_data);
        }
    }

    public void Initialize(DistrictCardData data)
    {
        if (data == null)
        {
            return;
        }

        _data = data;

        for (int i = 0; i < data.Cost; i++)
        {
            var goldCoin = Instantiate(_goldCoinPrefab, _costHolder.transform);
            _goldCoinInstances.Add(goldCoin);
        }

        if (_animator.isInitialized && _animator.gameObject.activeSelf)
        {
            _animator.SetBool(ANIMATOR_IS_VISIBLE, _data != null);
        }

        RefreshUI();
    }

    private void Reset()
    {
        foreach (var instance in _goldCoinInstances)
        {
            Destroy(instance.gameObject);
        }

        _goldCoinInstances.Clear();
    }

    private void RefreshUI()
    {
        _nameLocalization.StringReference.TableEntryReference = $"DISTRICT_{_data.Id}_NAME";
        _descriptionLocalization.StringReference.TableEntryReference = $"DISTRICT_{_data.Id}_DESCRIPTION";
    }

    public void OnClick()
    {
        OnSelected?.Invoke(_data);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            OnRequestPreview?.Invoke(_data);
        }
    }

    private void OnRectTransformDimensionsChange()
    {
        RefreshCostPart();
    }

    public void RefreshCostPart()
    {
        var rect = (transform as RectTransform).rect;
        float maxWidth = rect.width;
        float maxHeight = rect.height;

        var count = _costHolder.transform.childCount;
        float height = maxHeight / (count + (count * _costSpacingPercentage) + _costSpacingPercentage);
        float spacingPixels = height * _costSpacingPercentage;

        float aspectRatio = 1f;
        maxWidth = Mathf.Min((1f / aspectRatio) * height, maxWidth);

        _costHolder.spacing = spacingPixels;

        foreach (Transform child in _costHolder.transform)
        {
            var rectTransform = child as RectTransform;
            rectTransform.sizeDelta = new Vector2(maxWidth, rectTransform.rect.height);
        }
    }
}
