﻿using Boo.Lang;
using TMPro;
using UnityEngine;

public class PlayerBoardView : MonoBehaviour
{
    #region Serialized fields

    [Header("Inner references")]

    [SerializeField]
    private TMP_Text _nicknameText = null;

    [SerializeField]
    private TMP_Text _goldText = null;

    [SerializeField]
    private TMP_Text _crownText = null;

    [SerializeField]
    private RoleCardView _roleCardView = null;

    [SerializeField]
    private HorizontalCardHolderView _handCardsHolder = null;

    [Header("Assets")]

    [SerializeField]
    private DistrictCardView _districtCardPrefab = null;

    #endregion

    #region Private fields

    private PlayerGameSessionData _player;
    private List<DistrictCardView> _districtCardInstances = new List<DistrictCardView>();

    #endregion

    #region Properties

    public PlayerGameSessionData Player => _player;

    #endregion

    public void Initialize(PlayerGameSessionData player)
    {
        Reset();

        _player = player;

        RefreshUI();
    }

    private void Reset()
    {
        foreach (var instance in _districtCardInstances)
        {
            Destroy(instance.gameObject);
        }

        _districtCardInstances.Clear();
    }

    public void RefreshUI()
    {
        _nicknameText.text = _player.Nickname;
        _crownText.text = _player.HasCrown ? "K" : "";

        RefreshGold();
        RefreshRole();
        RefreshHand();
    }

    public void RefreshRole()
    {
        _roleCardView.Initialize(_player.Role);
    }

    public void RefreshGold()
    {
        _goldText.text = _player.Gold.ToString();
    }

    public void RefreshHand()
    {
        Reset();

        foreach (var districtCard in _player.Hand)
        {
            DistrictCardView districtCardView = Instantiate(_districtCardPrefab, _handCardsHolder.transform);
            districtCardView.Initialize(districtCard.Data);

            _districtCardInstances.Add(districtCardView);
        }
    }
}
