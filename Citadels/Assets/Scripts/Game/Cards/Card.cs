﻿using System;

public abstract class Card
{
    private string _instanceId;

    public string InstanceId => _instanceId;

    public Card()
    {
        _instanceId = System.Guid.NewGuid().ToString("N");
    }

    public Card(string instanceId)
    {
        _instanceId = instanceId;
    }
}
