﻿using System;

[Serializable]
public class DistrictCard : Card
{
    private DistrictCardData _data;

    public DistrictCardData Data => _data;
    public string Id => _data.Id;

    public DistrictCard(DistrictCardData data) : base()
    {
        _data = data;
    }

    public DistrictCard(DistrictCardData data, string instanceId) : base(instanceId)
    {
        _data = data;
    }
}
