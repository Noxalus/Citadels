﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class DistrictDeck
{
    private List<DistrictCard> _cards = new List<DistrictCard>();

    public int Count => _cards.Count;

    public DistrictDeck()
    {
    }

    public DistrictDeck(List<DistrictCardData> cardsData)
    {
        foreach (var cardData in cardsData)
        {
            _cards.Add(new DistrictCard(cardData));
        }
    }

    public void Shuffle()
    {
        _cards = _cards.OrderBy(x => Random.value).ToList();
    }

    public List<DistrictCard> DrawCards(int count)
    {
        List<DistrictCard> allCards = new List<DistrictCard>(_cards);
        List<DistrictCard> cards = new List<DistrictCard>();

        for (int i = 0; i < count; i++)
        {
            var randomCard = allCards[Random.Range(0, allCards.Count)];
            cards.Add(randomCard);
            allCards.Remove(randomCard);
        }

        return cards;
    }

    public void DealCardsToPlayer(List<string> cardsInstanceIds, PlayerGameSessionData player)
    {
        foreach (var cardInstanceId in cardsInstanceIds)
        {
            var districtCard = _cards.FirstOrDefault(c => c.InstanceId == cardInstanceId);

            if (districtCard == null)
            {
                Debug.LogError($"Can't find card with instance ID: {cardInstanceId}");
            }

            player.AddDistrictCard(districtCard);
            _cards.Remove(districtCard);
        }
    }

    public string ToJson()
    {
        var cards = new Stack<DistrictCard>(_cards);
        List<DistrictCardJSON> districtCards = new List<DistrictCardJSON>();

        for (int i = 0; i < cards.Count; i++)
        {
            var card = cards.Pop();
            var districtCard = new DistrictCardJSON()
            {
                InstanceId = card.InstanceId,
                DistrictCardId = card.Id
            };

            districtCards.Add(districtCard);
        }

        var districtCardsDeck = new DistrictCardsDeckJSON()
        {
            Cards = districtCards
        };

        return JsonUtility.ToJson(districtCardsDeck);
    }

    public void FromJson(DistrictCardsDeckJSON json, GameDatabase database)
    {
        _cards.Clear();

        Debug.Log("Load district deck from JSON data");

        foreach (var card in json.Cards)
        {
            Debug.Log($"{card.InstanceId} => {card.DistrictCardId}");

            var districtCard = new DistrictCard(database.GetDistrict(card.DistrictCardId), card.InstanceId);
            _cards.Add(districtCard);
        }
    }
}
