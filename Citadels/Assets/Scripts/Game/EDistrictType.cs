﻿public enum EDistrictType
{
    None = 0,
    Noble = 1,
    Religious = 2,
    Trade = 3,
    Military = 4,
    Special = 5
}
