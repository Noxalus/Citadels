﻿using System;
using System.Collections.Generic;

[Serializable]
public struct DistrictCardsDeckJSON
{
    public List<DistrictCardJSON> Cards;
}
