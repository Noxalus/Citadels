﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct DistrictCardJSON
{
    public string InstanceId;
    public string DistrictCardId;
}
