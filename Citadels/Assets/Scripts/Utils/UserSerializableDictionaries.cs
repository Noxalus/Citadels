﻿using System;

[Serializable]
public class DisctrictCardCountDictionary : SerializableDictionary<DistrictCardData, int> { };

#if NET_4_6 || NET_STANDARD_2_0
    [Serializable]
    public class StringHashSet : SerializableHashSet<string> { }
#endif