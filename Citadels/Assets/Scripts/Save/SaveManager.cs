﻿using System.IO;
using UnityEngine;

public class SaveManager : Singleton<SaveManager>
{
    public const string FILENAME = "Save.json";

#if !(UNITY_WEBGL && !UNITY_EDITOR)
    [SerializeField]
    private PlayerData _playerData = null;
#endif

    public void Save()
    {
#if !(UNITY_WEBGL && !UNITY_EDITOR)
        string saveFolder = Application.dataPath;

#if UNITY_EDITOR
        saveFolder = Directory.GetParent(saveFolder).FullName;
#endif

        string filePath = Path.Combine(saveFolder, FILENAME);
        string json = JsonUtility.ToJson(_playerData);
        File.WriteAllText(filePath, json);
#endif
    }

    public void Load()
    {
#if !(UNITY_WEBGL && !UNITY_EDITOR)
        string json;
        string saveFolder = Application.dataPath;

#if UNITY_EDITOR
        saveFolder = Directory.GetParent(saveFolder).FullName;
#endif

        string filePath = Path.Combine(saveFolder, FILENAME);

        if (File.Exists(filePath))
        {
            json = File.ReadAllText(filePath);
        }
        else
        {
            PlayerData playerData = ScriptableObject.CreateInstance<PlayerData>();
            json = JsonUtility.ToJson(playerData);
        }

        JsonUtility.FromJsonOverwrite(json, _playerData);
#endif
    }
}
