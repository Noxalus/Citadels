﻿using UnityEngine;

public class DebugManager : MonoBehaviour
{
    [SerializeField]
    private DebugConsole _console = null;

    private void Start()
    {
        _console.Close();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            ToggleDebugConsole();
        }
    }

    private void ToggleDebugConsole()
    {
        if (_console.IsOpened)
        {
            _console.Close();
        }
        else
        {
            _console.Open();
        }
    }

    public void AddMessage(string message)
    {
        _console.AddMessage(message);
    }
}
