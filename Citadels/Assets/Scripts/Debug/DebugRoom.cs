﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class DebugRoom : MonoBehaviourPunCallbacks
{
#if UNITY_EDITOR
    [SerializeField]
    private PlayerData _playerData = null;
#endif

    [SerializeField]
    private GameManager _gameManager = null;

    private void Awake()
    {
#if UNITY_EDITOR
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.AutomaticallySyncScene = true;

            PhotonNetwork.LocalPlayer.NickName = _playerData.Nickname;
            PhotonNetwork.ConnectUsingSettings();
        }
#else
        Destroy(this);
#endif
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to master");

        string roomName = "DebugRoom";

        RoomOptions options = new RoomOptions
        {
            MaxPlayers = 8,
            PublishUserId = true
        };

        PhotonNetwork.CreateRoom(roomName, options, null);
    }

    public override void OnJoinedRoom()
    {
        _gameManager.Start();
    }
}
