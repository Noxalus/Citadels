﻿using Boo.Lang;
using System;
using TMPro;
using UnityEngine;

public class DebugConsole : MonoBehaviour
{
    [SerializeField]
    private RectTransform _messagesHolder = null;

    [SerializeField]
    private TMP_Text _debugMessagePrefab = null;

    [SerializeField]
    private CanvasGroup _canvasGroup = null;

    private bool _isOpened;
    private List<TMP_Text> _debugMessageInstances = new List<TMP_Text>();

    public bool IsOpened => _isOpened;

    private void Start()
    {
        Reset();
    }

    private void Reset()
    {
        foreach (var instance in _debugMessageInstances)
        {
            Destroy(instance.gameObject);
        }

        _debugMessageInstances.Clear();
    }

    public void Open()
    {
        _canvasGroup.alpha = 1f;
        _canvasGroup.interactable = true;
        _canvasGroup.blocksRaycasts = true;
        _isOpened = true;
    }

    public void Close()
    {
        _canvasGroup.alpha = 0f;
        _canvasGroup.interactable = false;
        _canvasGroup.blocksRaycasts = false;
        _isOpened = false;
    }

    public void AddMessage(string message)
    {
        var debugMessage = Instantiate(_debugMessagePrefab, _messagesHolder);
        debugMessage.text = $"{DateTime.Now:HH:mm:ss} - {message}";

        _debugMessageInstances.Add(debugMessage);
    }
}
