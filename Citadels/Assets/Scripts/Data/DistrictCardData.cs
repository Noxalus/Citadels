﻿using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "DistrictCardData.asset", menuName = "Citadels/GameData/DistrictCardData")]
public class DistrictCardData : ScriptableObject, ICardData
{
    #region Serialized fields

    [SerializeField]
    private string _id = "";

    [SerializeField]
    private int _cost = 0;

    [SerializeField]
    private EDistrictType _type = EDistrictType.None;

    #endregion

    #region Properties

    public string Id => _id;
    public int Cost => _cost;
    public EDistrictType Type => _type;

    #endregion
}