﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData.asset", menuName = "Citadels/Save/PlayerData")]
public class PlayerData : ScriptableObject
{
    [SerializeField]
    private string _nickname = "";

    public string Nickname => _nickname;

    public void SetNickname(string value)
    {
        _nickname = value;
        SaveManager.Instance.Save();
    }

#if UNITY_EDITOR
    public void Reset()
    {
        _nickname = "";
    }
#endif
}
