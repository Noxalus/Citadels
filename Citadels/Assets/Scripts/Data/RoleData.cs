﻿using UnityEngine;

[CreateAssetMenu(fileName = "RoleData.asset", menuName = "Citadels/GameData/RoleCardData")]
public class RoleData : ScriptableObject
{
    #region Serialized fields

    [SerializeField]
    private string _id = "";

    [SerializeField]
    private int _rank = 0;

    [SerializeField]
    private Texture2D _background = null;

    #endregion

    #region Properties

    public string Id => _id;
    public int Rank => _rank;
    public Texture2D Background => _background;

    #endregion
}