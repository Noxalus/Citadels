﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "GameDatabase.asset", menuName = "Citadels/GameData/GameDatabase")]
public class GameDatabase : ScriptableObject
{
    #region Serialized fields

    [SerializeField]
    private List<RoleData> _roles = new List<RoleData>();

    [SerializeField]
    private List<DistrictCardData> _districts = new List<DistrictCardData>();

    #endregion

#if UNITY_EDITOR
    public void OnValidate()
    {
        _roles = _roles.Distinct().ToList();
        _districts = _districts.Distinct().ToList();
    }
#endif

    public RoleData GetRole(string id)
    {
        return _roles.First(role => role.Id == id);
    }

    public DistrictCardData GetDistrict(string id)
    {
        return _districts.First(district => district.Id == id);
    }
}
