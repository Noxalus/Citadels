﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "GameConfigurationData.asset", menuName = "Citadels/GameConfigurationData")]
public class GameConfigurationData : ScriptableObject
{
    #region Serialized fields

    [SerializeField]
    private List<RoleData> _availableRoles = new List<RoleData>();

    [SerializeField]
    private DistrictDeckData _districtCardsDeck = null;

    #endregion

    #region Properties

    public int RolesCount => _availableRoles.Count;

    #endregion

#if UNITY_EDITOR
    public void OnValidate()
    {
        // Remove duplicates
        _availableRoles = _availableRoles.Distinct().ToList();

        // Make sure we don't have more than 8 roles
        if (_availableRoles.Count > 8)
        {
            var count = _availableRoles.Count;
            for (int i = 0; i < count - 8; i++)
            {
                _availableRoles.RemoveAt(count - i);
            }
        }
    }
#endif

    // It's important to not give access to the _availableRoles collection directly
    // as it could be updated by the code and alter the real data in the .asset file
    public RoleData GetRole(int rank)
    {
        return _availableRoles.First(role => role.Rank == rank);
    }

    public DistrictDeckData GetDistrictCardsDeck()
    {
        return _districtCardsDeck;
    }
}
