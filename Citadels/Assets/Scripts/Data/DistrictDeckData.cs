﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DistrictDeck.asset", menuName = "Citadels/Deck/DistricDeck")]
public class DistrictDeckData : ScriptableObject
{
    [SerializeField]
    private DisctrictCardCountDictionary _districtCards = new DisctrictCardCountDictionary();

    public List<DistrictCardData> GetAllCards()
    {
        List<DistrictCardData> cards = new List<DistrictCardData>();

        foreach (var cardCountPair in _districtCards)
        {
            for (int i = 0; i < cardCountPair.Value; i++)
            {
                cards.Add(Instantiate(cardCountPair.Key));
            }
        }

        return cards;
    }
}
